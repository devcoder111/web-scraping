

## How do I get set up?

Download Chrome Driver and put into chromedriver/chromedriver.exe
http://chromedriver.chromium.org/downloads

---

### Install python, pip, virtualenv

Download python3.5.2 & pip
https://www.python.org/downloads/
Install virtualenv pip install virtualenv

---

### Install python packages

This programs requires the installation of Python with the following packages: time, csv, json, a os.path, pytz, BeautifulSoup, selenium
to install BeautifulSoup, selenium you must run the following console command:
$pip install BeautifulSoup
$pip install selenium

---

### Run script

python treasury.py